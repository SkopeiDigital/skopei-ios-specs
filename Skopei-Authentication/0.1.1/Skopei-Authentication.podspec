#
# Be sure to run `pod lib lint SkopeiAuthentication.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-Authentication"
  s.version          = "0.1.1"
  s.summary          = "A library that serves views and business logic regarding authentication."

  s.description      = "Use the Skopei Authentication library if your project needs any form of user authentication."

  s.homepage         = "https://bitbucket.org/SkopeiDigital/skopei-ios-authentication"
  s.license          = 'MIT'
  s.author           = { "Jordy van Kuijk" => "jordy@skopei.com" }
  s.source           = { :git => "git@bitbucket.org:SkopeiDigital/skopei-ios-authentication.git", :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.ios.resource_bundle = { 'SkopeiAuthentication' => 'Pod/Assets/**/*'}
  s.module_name = 'SkopeiAuthentication'

  s.dependency 'Skopei-Utils'
  s.dependency 'Skopei-API'
  s.dependency 'Skopei-Datamodels'
  s.dependency 'Alamofire'
  
end

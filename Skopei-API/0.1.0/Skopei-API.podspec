#
# Be sure to run `pod lib lint Skopei-API.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-API"
  s.module_name      = "SkopeAPI"
  s.version          = "0.1.0"
  s.summary          = "The Skopei-API pod contains convenience methods for communicating with an external API."

  s.description      = <<-DESC
                        Use the Skopei-API pod if you want to make use of convenience methods to connect to external APIs.
                        The pod supports basic security and url-connection abstractions. For a more detailed list of features,
                        please check the Readme.
                       DESC

  s.homepage         = "https://bitbucket.org/skopei/skopei-api"
  s.license          = 'Proprierty'
  s.author           = { "Jordy van Kuijk" => "jordy@kineticvision.nl" }
  s.source           = { :git => "https://Kukiwon@bitbucket.org/skopei/skopei-api.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'Skopei-API' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

#
# Be sure to run `pod lib lint Skopei-Datamodels.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-Datamodels"
  s.version          = "0.1.1"
  s.summary          = "This repository contains all the Data Models used throughout Skopei iOS applications."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = "This repository contains all the Data Models used throughout Skopei iOS applications. These models are used for the basis of the data models throughout the applications that use it."

  s.homepage         = "https://bitbucket.org/skopei/skopei-ios-datamodels/overview"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = '-'
  s.author           = { "Jordy van Kuijk" => "jordy@kineticvision.nl" }
  s.source           = { :git => "https://Kukiwon@bitbucket.org/skopei/skopei-ios-datamodels.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'Skopei-Datamodels' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

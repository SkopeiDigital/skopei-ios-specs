#
# Be sure to run `pod lib lint Skopei-Datamodels.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-Datamodels"
  s.version          = "0.1.5"
  s.summary          = "This repository contains all the Data Models used throughout Skopei iOS applications."
  s.description      = "This repository contains all the Data Models used throughout Skopei iOS applications. These models are used for the basis of the data models throughout the applications that use it."

  s.homepage         = "https://bitbucket.org/skopei/skopei-ios-datamodels/overview"
  s.license          = '-'
  s.author           = { "Jordy van Kuijk" => "jordy@kineticvision.nl" }
  s.source           = { :git => "https://Kukiwon@bitbucket.org/skopei/skopei-ios-datamodels.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.module_name = 'SkopeiDataModels'
  s.resources = ['Pod/Assets/*']

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Skopei-Utils'
  s.dependency 'Alamofire'
end

#
# Be sure to run `pod lib lint Skopei-Utils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-Utils"
  s.version          = "0.2.3"
  s.summary          = "Utility library that can be used throughout Skopei projects."

  s.description      = "The Skopei Utility library provides various extensions and utility functions that can be used throughout Skopei projects."

  s.homepage         = "https://Kukiwon@bitbucket.org/skopei/skopei-ios-utils.git"
  s.license          = 'MIT'
  s.author           = { "Jordy van Kuijk" => "jordy@kineticvision.nl" }
  s.source           = { :git => "https://Kukiwon@bitbucket.org/skopei/skopei-ios-utils.git", :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.ios.resource_bundle = { 'SkopeiUtils' => 'Pod/Assets/**/*'}
  s.module_name = 'SkopeiUtils'
  s.dependency 'ObjectMapper'
  s.dependency 'Alamofire'
  s.dependency 'AlamofireObjectMapper'
  s.dependency 'AlamofireImage'
end

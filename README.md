# README #

This is the Skopei CocoaPod spec. It contains all the modules that are used throughout Skopei projects for iOS. 

## Getting started
### Private Pods
Skopei uses private pods for use within CocoaPods projects. It is recommended that you read the following guide before you get started: [private cocoapods](https://guides.cocoapods.org/making/private-cocoapods.html)

### How do I push to this Spec?
If you want to push a new Pod or an updated pod to this spec, follow the steps below:

#### 1) Make sure you add the current Spec to your Pod installation:
```
#!python

pod repo add skopei-specs git@bitbucket.org:SkopeiDigital/skopei-ios-specs.git
```

#### 2) Lint your pod before pushing any new code to the Spec
Run `pod lib lint` and see if your pod validates. You might need to run it using this repo as a source like so:
```
pod lib lint --verbose --sources='git@bitbucket.org:SkopeiDigital/skopei-ios-specs.git,https://github.com/CocoaPods/Specs'
```

#### 3) Push the pod
**Don't forget to push a new tag (with a bumped) version to your pod repo first!**

run `pod repo push skopei-specs` and you should be done!
#
# Be sure to run `pod lib lint Skopei-Ticketing.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Skopei-Ticketing"
  s.module_name      = "SkopeiTicketing"
  s.version          = "0.2.2"
  s.summary          = "The Skopei-Ticketing pod contains all ticketing-related api-calls, business logic and views for you use in your app."
  s.description      = <<-DESC
                        Contains API-methods, business logic and views related to ticketing.
                        Supports user login, listing of tickets, creation and modification of new tickets.
                       DESC
  s.homepage         = "https://bitbucket.org/SkopeiDigital/skopei-ios-ticketing"
  s.license          = 'Proprietary'
  s.author           = { "Jordy van Kuijk" => "jordy@kineticvision.nl" }
  s.source           = { :git => "git@bitbucket.org:SkopeiDigital/skopei-ios-ticketing.git", :tag => s.version.to_s }

  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.ios.resource_bundle = { 'SkopeiTicketing' => 'Pod/Assets/**/*'}

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Skopei-Utils'
  s.dependency 'Skopei-Datamodels'
  s.dependency 'Skopei-API'
  s.dependency 'SwiftQRCode'
  s.dependency 'FSImageViewer', '~> 3.4'
  s.dependency 'AlamofireImage'

end
